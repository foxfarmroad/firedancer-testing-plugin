package firedancer.plugin.testing

import nebula.test.PluginProjectSpec

class FiredancerTestingPluginSpec extends PluginProjectSpec {
    @Override
    String getPluginName() {
        return 'firedancer.testing-plugin'
    }
}
