package firedancer.plugin.testing

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.testing.Test

class FiredancerTestingExtension {
    String unitTest = 'firedancer.junit.categories.UnitTest'
    String componentTest = 'firedancer.junit.categories.ComponentTest'
    String manualTest = 'firedancer.junit.categories.ManualTest'
}

class FiredancerTestingPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.plugins.apply(JavaPlugin)
        project.extensions.create('firedancerTesting', FiredancerTestingExtension)

        project.tasks.withType(Test) {
            useJUnit {
                excludeCategories project.firedancerTesting.manualTest
            }
        }

        def unitTestTask = project.tasks.create('unitTest', Test.class)
        unitTestTask.group = 'Testing'
        unitTestTask.description = 'Runs unit tests'
        unitTestTask.useJUnit {
            includeCategories project.firedancerTesting.unitTest
        }

        def componentTestTask = project.tasks.create('componentTest', Test.class)
        unitTestTask.group = 'Testing'
        unitTestTask.description = 'Runs component tests'
        unitTestTask.useJUnit {
            includeCategories project.firedancerTesting.componentTest
        }

        def manualTestTask = project.tasks.create('manualTest', Test.class)
        unitTestTask.group = 'Testing'
        unitTestTask.description = 'Runs manual tests'
        unitTestTask.useJUnit {
            includeCategories project.firedancerTesting.manualTest
        }
    }
}